\documentclass[12pt,journal,compsoc,fleqn]{IEEEtran}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{cite}
\usetikzlibrary{shapes,shadows,arrows}

\begin{document}
\tikzstyle{line} = [draw, -stealth, thick]
\tikzstyle{block} = [draw, rectangle, text width=5em, text centered, minimum height=8mm, node distance=8em]
\tikzstyle{block2} = [draw, rectangle, text width=6em, text centered, minimum height=8mm, node distance=4em]

\title{Visual Pose Estimation for a Mobile Manipulator}

\author{Mrinal Mohit and Shushman Choudhury% <-this % stops a space
\IEEEcompsocitemizethanks
{\IEEEcompsocthanksitem The authors are with the Indian Institute of Technology Kharagpur, India. mrinal.mohit@iitkgp.ac.in, shushman.choudhury@cse.iitkgp.ernet.in}}
\IEEEcompsoctitleabstractindextext{%
\begin{abstract}
This report describes the contributions of the authors in the development of the vision pipeline for HERB - a bimanual robotic manipulator. We describe the architecture of the vision system, pose estimation using AprilTags and tracking of the same using Extended Kalman Filters. We also discuss our work towards developing ROCK (Robust Object Category and Kinematic Pose), a fast and efficient visual classification and pose estimation algorithm.
\end{abstract}
\begin{IEEEkeywords}
Pose Estimation, AprilTags, Kalman Filtering, ROCK, HERB 
\end{IEEEkeywords}}
\maketitle
\IEEEdisplaynotcompsoctitleabstractindextext
\IEEEpeerreviewmaketitle
\section{Introduction}
\IEEEPARstart{T}{he} objective of our work and research was to formulate and develop the vision system for HERB (Home Exploring Robotic Butler) \cite{herb2} \cite{herb1}, a bimanual mobile manipulator. HERB is intended to perform useful tasks in general domestic environments, which lack the structure and predictability of factory environments. Accordingly, it needs a vision system that is comprehensive enough to identify objects of interest in clutter, is robust against failure, and is fairly efficient so as to provide actionable information to the other modules.

\section{Overview of Hardware Platform}
\begin{figure}[h]
\centering
	\includegraphics[scale=0.3]{HERB2}
	\caption{The Hardware Platform - HERB}
\label{fig:herb}
\end{figure}
At the Personal Robotics Lab, a platform was needed that could operate synergistically with humans to perform tasks in the home. The design of HERB, therefore, reflects the research interest in human-aware two-arm manipulation of unstructured environments. HERB's hardware allows it to
navigate indoors for hours at a time, sense its surroundings, and manipulate objects of interest for or with human partners, with minimal reliance on supporting infrastructure.

HERB's base comprises a Segway mobility platform, and it manipulates its environment with a pair of Barrett 7-DOF WAM arms with Barrett hands. 
Non-visual sensing is enabled by an array of four laser rangefinders just above ground level, odometric sensors in actuators and tactile sensors on the end-effectors. Computing is provided by 3 on-board high performance PCs while an ARM microcontroller is used for low-level hardware control. The vision hardware is composed of a high-sensitivity monochrome camera by Allied Vision for texture data, and an Asus Xtion Pro Live for depth sensing.

The system can be remotely controlled over the network through ROS (Robot Operating System), and all modules for the robot (perception, planning, execution etc.) are based on the ROS nodes and topics architecture.

\section{Vision Pipeline}
\begin{figure}[b]
\centering
	\includegraphics[scale=0.35]{usecase}
	\caption{Object detection for grasping - a use case}
\label{fig:usecase}
\end{figure}
The primary requirement of HERB's vision system currently is to provide perceptual information to the planning and manipulation modules. Therefore, this information would be in the form of pose estimates of important objects in HERB's current field of view, or which were seen recently. 

In computer vision, pose is defined as the combination of an object's position and orientation, relative to some coordinate system, represented by a translation and rotation transformation respectively. Notationally, 
\[
P = [x~y~z~q_x~q_y~q_z~q_w]^T 
\]
when the rotation is expressed as a quaternion. Alternatively the rotation can be expressed as a matrix, in which case the pose would be expressed as the 4$\times$4 matrix  
\[
P = 
\begin{bmatrix}
	R_{3\times3} & T_{1\times3} \\
	0_{3\times1} & 1
\end{bmatrix}
\] where $R$ is the rotation matrix and $T$ is the translation vector.

HERB's image sensors provide the depth information of the immediate environment, as well as a high-definition video feed for obtaining color and texture information. We have envisaged HERB's vision system to comprise different modules that can run parallelly. Each of these modules obtains the image information from HERB's sensors and then applies its own algorithm to obtain relevant pose information. This information is then available on request to other modules, to use as they see fit. A schematic diagram is shown in Fig \ref{fig:pipeline}
\begin{figure}[h]
\centering
	\begin{tikzpicture}
	\node [block] (sensors) {Vision Sensors};
	\node [block, right of=sensors, yshift=4.5em] (apriltags) {AprilTags};
	\node [block, right of=sensors, yshift=-1.5em] (linemod) {LINEMOD};
	\node [block, right of=sensors, yshift=1.5em] (rock) {ROCK};
	\node [block, right of=sensors, yshift=-4.5em] (moped) {MOPED};
	\node [block, right of=rock, yshift=-1.5em] (tracking) {Tracking Module};
		
	\draw [very thick,->] (sensors.east) to node[sloped, above, near end] {Images} (apriltags.west);
	\draw [very thick,->] (sensors.east) to (rock.west);
	\draw [very thick,->] (sensors.east) to (linemod.west);
	\draw [very thick,->] (sensors.east) to (moped.west);
	\draw [very thick,->] (apriltags.east) to node[sloped, above, midway] {Poses} (tracking.west);
	\draw [very thick,->] (rock.east) to (tracking.west);
	\draw [very thick,->] (moped.east) to (tracking.west);
	\draw [very thick,->] (linemod.east) to (tracking.west);
	
	\node [block, below of=tracking] (planning) {Planning Module};
	\draw [very thick,->] (tracking.south) to node[below] {Filtered Output} (planning.north);
	\end{tikzpicture}
\caption{The Vision Pipeline}
\label{fig:pipeline}
\end{figure}

Our work progressed in a number of phases, each of which is described in the subsequent sections.

\section{Detection and Tracking of AprilTags}
\begin{figure}[h]
\centering
	\includegraphics[scale=0.35]{apriltags}
	\caption{Sample AprilTags}
\label{fig:tags}
\end{figure}
AprilTags \cite{apriltags} is a visual fiducial system that can be useful for a number of applications. The tags can be detected quickly and reliably through cameras, and for our purpose, the pose estimates of the surfaces on which tags are attached, can be obtained. They have the advantage of being robust to lighting and angle. The existing implementation of AprilTags was ported onto our system for use.

Furthermore, in view of the fact that HERB would often need information about moving objects, and thereby moving tags, and also to prevent the loss of tags for a few frames when occlusion occurs, a tracking module for the tags, with persistence against frame-loss, was developed. We used a simple Extended Kalman Filter (EKF) model \cite{kalman} for this purpose. 

\subsection{EKF Model}
Since the observations were of the pose estimate, but we were maintaining information that could predict it's pose in the absence of observations, the measurement was of the form 
\[
z = [x~y~z~q_x~q_y~q_z~q_w]^T
\]
i.e. the pose estimate of the tag in translation-quaternion format. Meanwhile, the state estimate of the tag was of the form
\[ 
\hat{x} = [x~y~z~q_x~q_y~q_z~q_w~\dot{x}~\dot{y}~\dot{z}~\omega_x~\omega_y~\omega_z]
\] where the seven variables for the measurement are followed by the linear velocity ($\dot{x}$ etc.) and the angular velocity ($\omega_x$ etc.)

The model was based on the usual EKF formulation of a two-step cycle of prediction (before a measurement), and (subsequent) correction:
\[
\hat{x_k}^{-} = f(\hat{x_{k-1}})
\]
where $f$ is the state transition model, $\hat{x_{k-1}}$ is the state after the previous time step, and $\hat{x_k}^{-}$ is the next predicted state.  After the measurement $z_{k}$ is obtained, the new state is obtained as follows:
\[
\hat{x_k} = K_k.z_k + (1-K_k).\hat{x_k}^{-}
\]
where $K_k$ is a quantity called the Kalman Gain that is computed internally, based on the noise parameters of the model.

\subsection{Tag Persistence}
The tracking module was designed so as to persistently maintain the state of a tag that is being tracked when it is lost for a few observations. The predicted state of the tag is projected forward, as the error of the estimate magnifies. The number of frames for which persistence can last is a parameter that the user may set, after which the tracker for that tag becomes idle. 

\subsection{Pose Ambiguity Resolution}
At certain perspectives, i.e. when the tag is not sufficiently skewed with respect to the camera, there are ambiguities in the solutions for the pose generated by the detection module. This results in two pose results being mathematically plausible, and the output of the module switching between the two, randomly, in successive frames. 

This problem was tackled by leveraging two ideas - the first was the notion that ambiguous pose estimates would have similar re-projections in the camera frame, thereby providing a means to distinguish between a switched pose and the actual movement of the tags. 

The other was using a measurement validation gate to disallow the degradation of the state estimate from the Kalman Filter by an erroneous measurement. This requires the computation of a value called the normalized
innovation squared,
\[
\epsilon_v(k) = v(k)^TS(k)^{-1}v(k)
\]
where $v(k)$ is the difference between the estimated state and the observation and $S(k)$ is a procedural matrix. If this value is out of (user-defined) bounds, then the measurement is rejected. 

\subsection{Preliminary Results}
The developed module was used to track tags based on the EKF, remember them for some frames in case of data loss and discard them if they are lost for too long. This resulted in a reduction of the average error in detections and improved the stability of the tag detections. 

The module was used as part of the movie ``Robots 3D'' a segment of which was shot by National Geographic in our lab. HERB's task was to clear up a dinner table by locating, grasping and manipulating them. Tags were fixed on objects to facilitate their localization in world space.

\section{ROCK Detector}
\begin{figure}[h]
\centering
	\begin{tikzpicture}
	\node [block2] (detector) {Detector};
	\node [block2, below of=detector] (accumulator) {Accumulator};
	\node [block2, below of=accumulator] (tracker) {Tracker};
	\node [block2, above of=detector, xshift=-5em] (image) {Image};
	\node [block2, above of=detector, xshift=5em] (priors) {Priors};
		
	\draw [very thick,->] (image.east) to (priors.west);
	\draw [very thick,->] (image.south) to (detector.north);
	\draw [very thick,->] (priors.south) to (detector.north);
	\draw [very thick,->] (detector.south) to node[right]{Pose Hypotheses} (accumulator.north);
	\draw [very thick,->] (accumulator.south) to node[right]{Pose Decisions} (tracker.north);
	\draw [very thick,->, looseness = 1.75] (tracker.south) to [out=230,in=180]node[left]{Pose Estimates}(detector.west);
	\end{tikzpicture}
\caption{ROCK Workflow}
\label{fig:rock}
\end{figure}
ROCK (Robust Object Category and Kinematic Pose) is a method for visual recognition and pose estimation of objects in a manner that is easily usable by HERB, and other platforms as well. A method is desired which is fairly lightweight, discriminative, tolerant to variance in viewpoint and illumination, can incorporate strong priors, and can have an adjustable trade-off between accuracy and complexity.
As is evident from Fig. \ref{fig:rock}, ROCK has a number of different modules, each with its own nuances and challenges. Our work was related to a certain portion of those modules.

\subsection{Object Models}\label{subsec:models}
ROCK makes use of a database of object models, for recognition as well as pose estimation. The structure of these have been formulated for easy storage and processing of several dozen of them at a time, facilitating scalability.

We used Autodesk Inc's free software-service 123D Catch\texttrademark~ to generate fairly accurate and detailed three dimensional models from photographs of objects, shot under even lighting and fiducial backgrounds, from about two dozen perspectives (Fig \ref{fig:123d}). The software generates a mesh of vertices, edges and faces, which can be further refined and isolated from the background in 3D modelling packages. Furthermore, patches of the model are processed and information about them stored, for use in the subsequent testing phase.

\begin{figure}[b]
\centering
	\includegraphics[scale=0.5]{123d}
	\caption{3D Mesh from 2D photographs}
\label{fig:123d}
\end{figure}
\subsection{Pose Hypothesis Testing}
A number of pose hypotheses are obtained from the stage of generating priors, and now each of them needs to be tested for the strength of their response. This is achieved by projecting the sample pattern for the pose onto the image and testing the expectation.

The idea behind this is that the difference between corresponding patches (in LAB color space) is fairly stable with respect to illumination and rotation. During the model creation phase, information about the patches of the model had been stored. For a patch P defined by a bounding box, it is characterized by averaging the L,A,B channels values of the patch thus:
\[
P(l) \equiv mean\left(\sum\limits_{i=y_1}^{y_2} \sum\limits_{j=x_1}^{x_2} L(i,j)\right)
\]
$P(a)$ and $P(b)$ are calculated similarly, using the A and B channels respectively. Thereafter, a patch P is represented as:
\[
P = \left\{P(l),P(a),P(b)\right\}
\]
The difference between pairs of nearby patches on the model is stored as a binary vector
\[
d(P_1,P_2) = \begin{bmatrix}
			P_1(l)>P_2(l) \\
			P_1(a)>P_2(a) \\
			P_1(b)>P_2(b) 
		      \end{bmatrix}
\]
This is the vector that represents the relationship between a given pair of patches. The difference vectors for every such pair, is stored a priori for each model. For both training and test images, the patch information is computed quickly using the concept of integral images \cite{viola}. 

For each pose, the model is overlaid on the test image and the differences between patches are obtained and compared with the corresponding difference vectors of the model. The response (strength) of a pose hypothesis is therefore
\[
R_H = \sum \limits_{i} sim(M_{d_i},H_{d_i})
\]
where $sim$ refers to a similarity metric (cosine similarity, table lookup), $M_{d_i}$ refers to a difference vector for a pair of patches in the model, and $H_{d_i}$ for the hypothesis. 

A variety of similarity metrics are being explored for this purpose to account for noise, randomness and occlusion. For instance, a stricter check mechanism reduces the false positives but also increases the effect of noise.

The basic idea is to start testing with coarse samples and then progressively refine the promising solutions. Testing at finer layers takes more time but gives more accurate responses, as one would expect, and this accuracy/computation tradeoff is an interesting area of exploration.
\subsection{Pose Response Accumulation}
The first stages of ROCK generate a number of object pose hypotheses with their respective responses, as mentioned earlier. From this information, a decision needs to be made about the number of instances of a particular object, and the most likely pose of each. For this purpose, we use mean-shift clustering techniques. \cite{meanshift} \cite{meanshift3D} \cite{meanshift_pose}

We separate the pose hypotheses in terms of the objects they are associated with. Thereafter, for a set of pose hypotheses for a particular object, we first group them based on their position in 3D space, i.e. based on the translational component. This is done via mean-shift clustering with Euclidean distance. The bandwidth for this clustering is based on the known dimensions of the object, so as to disambiguate between hypotheses that must belong to different instances. Clustering in 3D space is a fairly fast procedure, and allows us to separate poses into groups of interest, by leveraging our prior knowledge about the object in question.

The next step is to analyze each of the small clusters obtained from the first step, and to identify if there is one or more instances of the object, and the best pose for each. For this, we use a non-linear mean-shift clustering for poses. The number of centres for each cluster indicates the number of instances that we believe are present, and the weighted average associated with each centre represents the accumulated pose hypothesis for that instance.

\subsection{GPU Parallelization}
The inherently independent nature of the patch comparisons and reprojections, for evaluation of the response of a test pose against an image for a given model, naturally incentivizes an exploration of parallel computing techniques to speeden up the process.

We made use of NVIDIA Corporation's CUDA\texttrademark~, a parallel computing architecture and API, implemented on Graphics Processing Units (GPUs), rather than on traditional processors. Although the platform has proven to be highly successful in general, certain restrictions on the expected organization of the data structures in order to minimize bottlenecked memory transfer delays required re-organization of the ROCK Detector module.

Preliminary testing has shown promising speed-ups but we are exploring further avenues for parallelized performance improvements.

\section{LINEMOD Extensions}
The recently proposed LINEMOD algorithm \cite{linemod} has fared well in generic rigid object detection, proceeding using very fast template matching. It combines multiple complementary modes (e.g. images and depth maps), is fast, and can handle untextured objects.

The algorithm was tested on the same objects as were used in the aforementioned detectors. Experiments resulted in high true positives (~85\%) and low false positives (<10\%) with high tolerance (~50\%) to occlusions. However, robust and effective detections require a high number of templates (2,000-10,000), leading to a time-consuming, tedious and unscaling model capture stage.

We thus investigated a procedure for automated generation of these templates from dense meshes (already built as described in Section \ref{subsec:models})

\subsection{Automated Template Generation}
We set up a 3D modeling and rendering package to capture color and depth images of objects so as to simulate the physical RGBD camera. For every object, a camera captures these frames from multiple viewpoints as observed from equally spaced points on different-sized spheres enclosing the object (Fig \ref{fig:dome}). These viewpoints approximate physically moving the camera around the object. We adapted LINEMOD to use these captured frames for model generation.

\begin{figure}[h]
\centering
	\includegraphics[scale=0.4]{dome}
	\caption{Object images are captured from the points shown on the green dome.}
\label{fig:dome}
\end{figure}

Although the LINEMOD detection responses were expectedly good on synthetic testsets, the real-world performance turned out to be worse than that from a smaller number of hand-captured templates. The number of false positives were unacceptably high (>60\%), and the algorithm slow down by an order of magnitude. The possible reasons for this inadequacy were proposed and evaluated - disparities between the actual and simulated camera frames, lack of fidelity in the generated meshes and idiosyncrasies in the template matching metric for simulated streams, but none of the hypthoses could satisfactorily explain the discrepancies. 

A newer work on LINEMOD \cite{linemodnew} embraces the automatic template generation concept, but has not been made publically available yet. We hope that our efforts would be reconciled with the approaches taken by the developers of LINEMOD.
\section{Miscellany}
We also contributed to a number of other tasks, some of which are outlined here.

\subsection{Camera Intrinsics Refinement}
The intrinsic parameters of a camera are based on the pinhole model, and affect how the camera forms images from the environment.

Computationally, the intrinsic parameters are represented as a matrix, called the camera matrix:
\[
C = \begin{bmatrix}
	f_x & 0 & c_x \\
	0 & f_y & c_y \\
	0 & 0 & 1
	\end{bmatrix}
\]
where $f_x$ and $f_y$ are the focal lengths, and $c_x$ and $c_y$ refer to the co-ordinates of the image.

Accurate intrinsics are important for correct transformations from the 2D image space to the 3D world space. With a mixture of calibration methods and parameter tuning, we were able to enhance the performance of HERB's cameras.
\subsection{Extrinsic Calibration of Vision Sensors}
In order for HERB to interact accurately with objects in the real world, it is important for the relative positions of certain components of HERB, to be accurately known. The same applies to the vision sensors.

We wish to obtain $T_{AC}$, the transformation from the actuator to camera, which is fixed and unknown. The idea behind this was to obtain visual samples of markers in the world from different camera positions, and knowing that they are fixed with respect to the world, formulate an expression for the extrinsic parameters. The related terms are:\\\\
$T_{WA_n} \equiv$ World to actuator at time $n$ (known)\\
$T_{WC_n} \equiv$ World to camera at time $n$ (unknown)\\
$T_{WC_n} = T_{WA_n} * T_{AC}$\\
$T_{CM_n} \equiv$ Camera to marker at time $n$ (known)\\
$T_{WM_n}  \equiv$ World to marker at time $n$ (fixed; unknown)\\
$T_{WM_n} = T_{WC_n} * T_{CM_n}$\\

We will try to solve for $T_{AC}$, given several samples of $T{WA_n}$ and $T_{CM_n}$. Considering that the observed markers are fixed w.r.t the world, we obtain:
\begin{gather*}
T_{WM_1} = T_{WM_2}\\
\implies T_{WC_1} * T_{CM_1} = T_{WC_2} * T_{CM_2}\\
\implies T_{WC_2}^{-1} * T_{WC_1} = T_{CM_2}*T_{CM_1}^{-1}\\
\implies \left(T_{WA_2}*T_{AC}\right)^{-1} * \left(T_{WA_1} * T_{AC}\right) = K_2\\
\implies T_{AC}^{-1} * \left(T_{WA2}^{-1} * T_{WA_1} \right) * T_{AC} = K_2\\
\implies T_{AC}^{-1} * K_1 * T_{AC} = K_2\\
\implies K_1 * T_{AC} = T_{AC} * K2\\
\end{gather*}

This is the well-known \textbf{$AX = XB$} problem formulation that arises for the calibration of sensors, and we set this up as a system of equations by solving for the individual elements of $T_{AC}$. 

\subsection{Pipeline Optimization }
Vision-based ROS nodes were modified such that they would be dormant unless subscribed to by an interested module. This lazy evaluation scheme led to savings in computation resources, and an increase in efficiency.

The detectors and the tracker were bundled into a common package, which launched automatically on robot start-up.
\section{Conclusion}
We have succeeded in fulfilling the objectives laid out for the duration of this internship. We were able to obtain valuable experience regarding the organization and implementation of the vision system of a robotic system.

The work on AprilTags led to important insights about how to track these visual fiducial markers, and enhanced their usability. Furthermore, we were able to apply our knowledge and directly contribute towards a working demonstration. We complemented the implementation aspect of our work with a principled approach towards the new framework we are helping to develop for visual recognition and kinematic pose estimation, ROCK. 

Finally, the auxiliary tasks we did by investigating existing systems such as LINEMOD, and working on the vision system components, will be important for other future applications.

\section*{Acknowledgments}
The authors would like to thank Prof. Siddhartha Srinivasa, masters student Aaron Walsman and the members of the Personal Robotics Lab for their guidance and advice, and the Robotics Institute Summer Scholar program for enabling this collaboration.
% Generated by IEEEtran.bst, version: 1.13 (2008/09/30)
\begin{thebibliography}{7}
\providecommand{\url}[1]{#1}
\csname url@samestyle\endcsname
\providecommand{\newblock}{\relax}
\providecommand{\bibinfo}[2]{#2}
\providecommand{\BIBentrySTDinterwordspacing}{\spaceskip=0pt\relax}
\providecommand{\BIBentryALTinterwordstretchfactor}{4}
\providecommand{\BIBentryALTinterwordspacing}{\spaceskip=\fontdimen2\font plus
\BIBentryALTinterwordstretchfactor\fontdimen3\font minus
  \fontdimen4\font\relax}
\providecommand{\BIBforeignlanguage}[2]{{%
\expandafter\ifx\csname l@#1\endcsname\relax
\typeout{** WARNING: IEEEtran.bst: No hyphenation pattern has been}%
\typeout{** loaded for the language `#1'. Using the pattern for}%
\typeout{** the default language instead.}%
\else
\language=\csname l@#1\endcsname
\fi
#2}}
\providecommand{\BIBdecl}{\relax}
\BIBdecl

\bibitem{herb2}
S.~Srinivasa, D.~Berenson, M.~Cakmak, A.~{Collet Romea}, M.~Dogar, A.~Dragan,
  R.~A. Knepper, T.~D. Niemueller, K.~Strabala, J.~M. Vandeweghe, and
  J.~Ziegler, ``Herb 2.0: Lessons learned from developing a mobile manipulator
  for the home,'' \emph{Proceedings of the IEEE}, vol. 100, no.~8, pp. 1--19,
  July 2012.

\bibitem{herb1}
S.~Srinivasa, D.~{Ferguson }, C.~Helfrich, D.~Berenson, A.~{Collet Romea},
  R.~Diankov, G.~Gallagher, G.~Hollinger, J.~Kuffner, and J.~M. Vandeweghe,
  ``Herb: a home exploring robotic butler,'' \emph{Autonomous Robots}, vol.~28,
  no.~1, pp. 5--20, January 2010.
  
\bibitem{apriltags}
E.~Olson, ``{AprilTag}: A robust and flexible visual fiducial system,'' in
  \emph{Proceedings of the {IEEE} International Conference on Robotics and
  Automation ({ICRA})}.\hskip 1em plus 0.5em minus 0.4em\relax IEEE, May 2011,
  pp. 3400--3407.

\bibitem{kalman}
R.~E. Kalman and R.~S. Bucy, ``{New results in linear filtering and prediction
  theory},'' \emph{Transactions of the ASME. Series D, Journal of Basic
  Engineering}, vol.~83, pp. 95--107, 1961.

 \bibitem{meanshift}
K.~Fukunaga and L.~Hostetler, ``The estimation of the gradient of a density
  function, with applications in pattern recognition,'' \emph{IEEE Trans. Inf.
  Theor.}, vol.~21, no.~1, pp. 32--40, Sep. 2006. [Online].

\bibitem{meanshift3D}
R. ~Subbarao, Y.~Genc and P.~Meer, "Nonlinear mean shift for robust pose estimation" \emph{Applications of Computer Vision, 2007. WACV'07}, Feb. 2007

\bibitem{meanshift_pose}
D.~Comaniciu and P~Meer, "Mean shift: A robust approach toward feature space analysis." \emph{IEEE Transactions on Pattern Analysis and Machine Intelligence}, vol.~24, no.~5, pp. 603-619, 2002

\bibitem{linemod}
S.~Hinterstoisser, S.~Holzer, C.~Cagniart, S.~Ilic, K.~Konolige, N.~Navab, and
  V.~Lepetit, ``Multimodal templates for real-time detection of texture-less
  objects in heavily cluttered scenes,'' 2011.
  
\bibitem{linemodnew}
S.~Hinterstoisser, V.~Lepetit, S.~Ilic, S.~Holzer, G.~Bradski, K.~Konolige, ,
  and N.~Navab, ``Model based training, detection and pose estimation of
  texture-less 3d objects in heavily cluttered scenes,'' 2012.

\bibitem{viola}
Viola, P., and Jones, M. (2001). Rapid object detection using a boosted cascade of simple features. In \emph{Computer Vision and Pattern Recognition, 2001. CVPR 2001. Proceedings of the 2001 IEEE Computer Society Conference on} (Vol. 1, pp. I-511). IEEE.

\end{thebibliography}

\end{document}


